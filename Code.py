import random
def main():
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("Text adventure!")
    print("Content warning: blood, darkness, death, candy")
    print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")
    print("")
    print("You're in a dark room. You find a candle and light it, and you see")
    print("two doors out of the room you're in right now. In the left door, you")
    print("can hear moaning of some kind out the other end. In the other,")
    print("rightmost door is humid on the other side and you can hear running")
    print("water, and it seems to have natural light.")
    print("")

    choice1 = input("> What door do you go through? right or left? * ")

    if choice1 == "right":
        print("You go through the right door. There is a wall, with a small hole.")
        print("You crawl through the hole, and you wind up in a large cavernous")
        print("room with a waterfall at the back. There is a ring of flowers around")
        print("the room, like a donut. In the middle of the room, there's a pedastel")
        print("with a candy bowl in it. A sign says \"Take one.\"")
        print("")
        candyfell = 0
        while 1 == 1:
            takeone = input("> Take a piece of candy? yes or no? * ")
            print("")
            if takeone == "yes":
                if candyfell == 0:
                    print("You take a piece of candy.")
                    print("You feel good about life.")
                if candyfell == 1:
                    print("You take a piece of candy.")
                    print("You feel like the scum of the world.")
                if candyfell == 2:
                    print("You take a piece of candy.")
                    print("You think about those poor kids who can't have candy,")
                    print("because you're taking so much. You're filled with guilt.")
                if candyfell == 3:
                    print("You try to take a piece of candy, but the bowl fell over.")
                    print("Look at what you've done.You leave this room. You return")
                    print("and go back to the other door.")
                    choice1 = "left"
                    break
            if takeone == "no":
                print("You leave this room. You return and go back to the other door.")
                choice1 = "left"
                break
            candyfell = random.randint(1,3)
    if choice1 == "left":
        print("You go through the door, you start to hear rumbling. The place is")
        print("going to collapse! Do you look for the moaning?")
        print("")
        moansearch = input("> yes or no? * ")
        if moansearch == "yes":
            print("You find the source: It's an old man sitting by the side of the tunnel.")
            print("He looks to be in great pain, and he has a white shirt on with")
            print("a big bloody mess on it. ")
            print("")
            helpthem = input("> Do you help him? yes or no? * ")
            print("")
            if helpthem == "yes":
                print("")
                print("You pick up the old man, firefighter style. A boost of")
                print("adrenaline gives you more strength than you would normally")
                print("have. You can see clearer, and you see several cracks in")
                print("the ground. That must have been where the old man fell. You")
                print("jump over these cracks, and see light at the end of the tunnel.")
                print("You jump out of the cave at the last minute, right before it")
                print("collapses. You wind up in a meadow, and a merchant picks you")
                print("both up and brings you to a town, where the old man is healed.")
                print("Good ending.")
            if helpthem == "no":
                print("You decide it's too risky to save the old man.")
                lastwords = input("> Is there anything you want to say to him? * ")
                print("")
                print("You say to the old man, \"" + lastwords + "\" and he spits on your")
                print("shoes. You start running, but your foot lands in a crevice")
                print("and you can't get it unstuck. You hear the old man laughing")
                print("at you as the cave collapses on both of you. Bad ending.")
        if moansearch == "no":
            print("")
            print("You try to run out of the cave, disregarding the moaning. It's")
            print("probably a bear or something anyway and you don't want to find")
            print("out. You fall in a crevice, however, and you see an old man")
            print("there, telling you to get away. You both die under the falling")
            print("rocks. Bad ending.")
main()
